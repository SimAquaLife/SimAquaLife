package fr.cemagref.simaqualife.kernel.processes;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.Processes;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess.Synchronisation;
import fr.cemagref.simaqualife.kernel.spatial.Environment;
import fr.cemagref.simaqualife.kernel.spatial.Position;
import fr.cemagref.simaqualife.kernel.util.TransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;
import java.lang.reflect.InvocationTargetException;

public class LoopAquaNismsGroupProcessTest extends TestCase {

    int nbInd = 10;
    int indToRemove = 2;

    class AquaNismTest extends AquaNism<Position, Environment> {

        int value;

        public AquaNismTest(Pilot pilot, int value) {
            super(pilot, null);
            this.value = value;
        }

        @Override
        public String toString() {
            return "" + value;
        }
    }

    class ProcessTest extends LoopAquaNismsGroupProcess<AquaNismTest, AquaNismsGroup<AquaNismTest, ?>> {

        boolean begin = false, end = false;

        public ProcessTest() {
            super();
        }

        public ProcessTest(Synchronisation syncMode) {
            super();
            super.synchronisationMode = syncMode;
        }

        @AtBegin
        public void beginTest() {
            begin = true;
        }

        @AtEnd
        public void endTest() {
            end = true;
        }

        @Override
        protected void doProcess(AquaNismTest aquanism, AquaNismsGroup<AquaNismTest, ?> group) {
            if (aquanism.value == indToRemove) {
                this.interAquanism(aquanism);
                this.addAquanism(new AquaNismTest(group.getPilot(), nbInd), group);
                // check that action are not commited immediatly if not in async mode
                if (this.synchronisationMode == Synchronisation.ASYNCHRONOUS) {
                    boolean newIndFound = false;
                    for (AquaNismTest ind : group.getAquaNismsList()) {
                        if (ind.value == indToRemove) {
                            fail();
                        }
                        if (ind.value == nbInd) {
                            newIndFound = true;
                        }
                    }
                    assertTrue(newIndFound);
                } else {
                    boolean indToRemoveFound = false;
                    for (AquaNismTest ind : group.getAquaNismsList()) {
                        if (ind.value == indToRemove) {
                            indToRemoveFound = true;
                        }
                        if (ind.value == nbInd) {
                            fail();
                        }
                    }
                    assertTrue(indToRemoveFound);
                }
            }
        }
    }

    class GroupProcessTest extends AquaNismsGroupProcess<AquaNismTest, AquaNismsGroup<AquaNismTest, ?>> {

        @Override
        public void doProcess(AquaNismsGroup<AquaNismTest, ?> group) {
            this.addAquanism(new AquaNismTest(group.getPilot(), -1), group);
        }
    }

    public void testInitMethodsLists() throws Exception {
        ProcessTest process = new ProcessTest();
        process.initAtBeginAndAtEndMethodsLists(new Pilot());
        assertTrue(process.methodsAtBegin.contains(ProcessTest.class.getDeclaredMethod("beginTest")));
        assertTrue(process.methodsAtEnd.contains(ProcessTest.class.getDeclaredMethod("endTest")));
        process.runAtBeginMethods();
        assertTrue(process.begin);
        process.runAtEndMethods();
        assertTrue(process.end);
    }

    public void testDoProcessANG() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        for (Synchronisation syncMode : Synchronisation.values()) {
            stepProcess(syncMode);
        }
    }

    public void stepProcess(Synchronisation syncMode) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Pilot pilot = new Pilot();
        ProcessTest process1 = new ProcessTest(syncMode);
        List<AquaNismsGroupProcess> processes2 = new ArrayList<AquaNismsGroupProcess>();
        processes2.add(process1);
        Processes processes = new Processes(new ArrayList<AquaNismsGroupProcess>(), processes2, new ArrayList<AquaNismsGroupProcess>());

        AquaNismsGroup<AquaNismTest, ?> group = new AquaNismsGroup<AquaNismTest, Environment>(new Pilot(), null, processes);

        try {
            TransientParameters.callAnnotedMethods(group, pilot);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

        for (int i = 0; i < nbInd; i++) {
            group.addAquaNism(new AquaNismTest(group.getPilot(), i));
        }

        group.step();
        group.commitPendingActions();

        // check that individual #toRemove has been removed and that the new has been added
        boolean newIndFound = false;
        for (AquaNismTest ind : group.getAquaNismsList()) {
            if (ind.value == indToRemove) {
                fail("Fails for mode " + syncMode);
            }
            if (ind.value == nbInd) {
                newIndFound = true;
            }
        }
        assertTrue(newIndFound);
        System.out.println(syncMode + " mode succeed !");
    }

}
