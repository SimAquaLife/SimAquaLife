package fr.cemagref.simaqualife.kernel;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.processes.LoopAquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.spatial.Environment;
import fr.cemagref.simaqualife.kernel.spatial.Position;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

// fait bugger TransientParameters.callAnnotedMethods
class AnEnvironment extends Environment<Position, AquaNism<Position, ?>> {

    @Override
    public void addAquaNism(AquaNism<Position, ?> aquaNism, AquaNismsGroup group) {
    }

    @Override
    public List<Position> getNeighbours(Position position) {
        return null;
    }

    @Override
    public void moveAquaNism(AquaNism<Position, ?> aquaNism,
            AquaNismsGroup group, Position destination) {
    }

    @Override
    public void removeAquaNism(AquaNism<Position, ?> aquaNism,
            AquaNismsGroup group) {
    }
    boolean init = false;

    @InitTransientParameters
    public void testInit(Pilot pilot) {
        init = true;
    }
}

class AnAquaNismsGroup extends AquaNismsGroup<AquaNism, AnEnvironment> {

    public AnAquaNismsGroup(Pilot pilot, AnEnvironment environment, Processes processes) {
        super(pilot, environment, processes);
    }
    boolean init = false;

    @InitTransientParameters
    public void testInit(Pilot pilot) {
        init = true;
    }
}

class ALoopProcess extends LoopAquaNismsGroupProcess<AquaNism, AnAquaNismsGroup> {

    boolean init = false;

    @InitTransientParameters
    public void testInit(Pilot pilot) {
        init = true;
    }

    @Override
    protected void doProcess(AquaNism aquanism, AnAquaNismsGroup group) {
    }
}

class AProcess extends AquaNismsGroupProcess<AquaNism, AnAquaNismsGroup> {

    boolean init = false;

    @InitTransientParameters
    public void testInit(Pilot pilot) {
        init = true;
    }

    @Override
    public void doProcess(AnAquaNismsGroup object) {
    }
}

public class AquaticWorldTest extends TestCase {

    public void testInit() {
        Pilot pilot = new Pilot();
        AquaticWorld<AnEnvironment, AquaNism, AquaNismsGroup<AquaNism, AnEnvironment>> world = new AquaticWorld<AnEnvironment, AquaNism, AquaNismsGroup<AquaNism, AnEnvironment>>();
        world.environment = new AnEnvironment();
        ALoopProcess process1 = new ALoopProcess();
        List<AquaNismsGroupProcess> processes1 = new ArrayList<AquaNismsGroupProcess>();
        processes1.add(process1);
        AProcess process2 = new AProcess();
        List<AquaNismsGroupProcess> processes2 = new ArrayList<AquaNismsGroupProcess>();
        processes2.add(process2);
        Processes processes = new Processes(processes1, processes2, new ArrayList<AquaNismsGroupProcess>());
        AnAquaNismsGroup group1 = new AnAquaNismsGroup(pilot, world.environment, processes);
        world.aquaNismsGroupsList = new ArrayList<AquaNismsGroup<AquaNism, AnEnvironment>>();
        world.aquaNismsGroupsList.add(group1);

        assertFalse(world.environment.init);
        assertFalse(process1.init);
        assertFalse(process2.init);
        assertFalse(group1.init);
        pilot.setAquaticWorld(world);
        try {
            world.init(pilot);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            fail();
        }

        assertTrue(world.environment.init);
        assertTrue(process1.init);
        assertTrue(process2.init);
        assertTrue(group1.init);
    }
}
