package fr.cemagref.simaqualife.pilot;

import fr.cemagref.ohoui.annotations.Description;

import java.io.File;

public class PilotParameters {

    @Description(name = "Duration", tooltip = "The duration of the simulation")
    long simDuration = 100;
    @Description(name = "Begin", tooltip = "The time when the simulation starts")
    long simBegin = 1;
    @Description(name = "timestep duration", tooltip = "The elapsed time during a step of the simulation")
    long timeStepDuration = 1;
    @Description(name = "List of observers", tooltip = "")
    protected File observersFile;
    @Description(name = "Status index", tooltip = "The index of status to init the RNG")
    int rngStatusIndex = 0;

    public final int getRngStatusIndex() {
        return rngStatusIndex;
    }

    public final long getSimBegin() {
        return simBegin;
    }

    public final long getSimDuration() {
        return simDuration;
    }

    public final long getTimeStepDuration() {
        return timeStepDuration;
    }

    public final File getObserversFile() {
        return observersFile;
    }
}
