package fr.cemagref.simaqualife.pilot;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import fr.cemagref.observation.kernel.ObservableManager;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.simaqualife.kernel.AquaticWorld;
import fr.cemagref.simaqualife.kernel.util.TransientParameters;
import fr.cemagref.simaqualife.pilot.rng.MRG32k3aWithCounter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import org.apache.commons.io.output.NullOutputStream;

import umontreal.iro.lecuyer.rng.MRG32k3a;

public final class Pilot {

    protected PilotParameters parameters = new PilotParameters();
    private AquaticWorld aquaticWorld = new AquaticWorld();
    private transient MRG32k3a randomStream;
    private final transient ObservableManager observableManager;
    private transient long currentTime;
    private transient boolean stopAtEndOfNextStep;
    private transient boolean stopped = true;
    private transient PrintStream outputStream = System.out;
    private transient XStream xstream;

    public Pilot() {
        observableManager = new ObservableManager();
        xstream = new XStream(new DomDriver());
    }

    public void setXstream(XStream xstream) {
        this.xstream = xstream;
    }

    public PrintStream getOutputStream() {
        return this.outputStream;
    }
    private transient boolean debug = false;

    public boolean isDebug() {
        return this.debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }
    
    public void setQuiet() {
        this.outputStream = new PrintStream(new NullOutputStream());
    }

    public PilotParameters getParameters() {
        return this.parameters;
    }

    public void setParameters(PilotParameters parameters) {
        this.parameters = parameters;
    }

    public MRG32k3a getRandomStream() {
        return this.randomStream;
    }

    public long getCurrentTime() {
        return this.currentTime;
    }

    public ObservablesHandler addObservable(Class cl) {
        return observableManager.addObservable(cl);
    }

    public void closeObservers() {
        observableManager.closeObservers();
    }

    public ObservableManager getObservableManager() {
        return observableManager;
    }

    public final AquaticWorld getAquaticWorld() {
        return this.aquaticWorld;
    }

    public final void setAquaticWorld(AquaticWorld world) {
        this.aquaticWorld = world;
    }

    public void setSimBegin(long simBegin) {
        this.parameters.simBegin = simBegin;
        this.currentTime = simBegin;
    }

    public long getSimBegin() {
        return this.parameters.simBegin;
    }

    public void setSimDuration(long simDuration) {
        Pilot.this.parameters.simDuration = simDuration;
    }

    public long getSimDuration() {
        return this.parameters.simDuration;
    }

    public void setSimTimeStepDuration(long timeStep) {
        this.parameters.timeStepDuration = timeStep;
    }

    public long getTimeStepDuration() {
        return this.parameters.timeStepDuration;
    }

    public void setObserversFile(File observersFile) {
        this.parameters.observersFile = observersFile;
    }

    public void setRNGStatusIndex(int statusIndex) {
        this.parameters.rngStatusIndex = statusIndex;
    }

    public void setOutputStream(PrintStream outputStream) {
        this.outputStream = outputStream;
    }

    public Class getEnvironmentClass() {
        assert this.aquaticWorld != null;
        return this.aquaticWorld.getEnvironment().getClass();
    }

    // TODO remove this when observer will be dynamically configurable
    public Class getAquaNismClass() {
        Type aquanismType = ((ParameterizedType) getEnvironmentClass().getGenericSuperclass()).getActualTypeArguments()[1];
        if (aquanismType instanceof ParameterizedType) // env is parametrized with an abstract type of AquaNism
        {
            return (Class) ((ParameterizedType) aquanismType).getRawType();
        } else {
            return (Class) aquanismType;
        }
    }

    public boolean isFinished() {
        return (this.currentTime >= this.parameters.simBegin + this.parameters.simDuration);
    }

    public boolean isStopped() {
        return this.stopped;
    }

    public void pause() {
        this.stopAtEndOfNextStep = true;
    }

    public void stop() {
        pause();
        if (!this.stopped) {
            this.stopped = true;
            // TODO wait the end of the current step
            //ObservableManager.closeObservers(); // FCh : Now it's done AquaticWorld.end() method
        }
    }

    public void lockAsStarted() { // FCh: Use for force simulation  according criterion (see HDPP model)
        this.stopAtEndOfNextStep = false;
        if (!this.stopped) {
            this.stopped = false;
        }
    }

    public void init() {
        this.randomStream = new MRG32k3aWithCounter();
    }

    /**
     * <code>load</code> This method achieve the simulation environment
     * initialization, and run the model init phase
     *
     * @throws FileNotFoundException if an error occurs while loading the RNG
     * status file
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public void load() throws FileNotFoundException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        this.randomStream.setSeed(new long[]{(long) 12345.0, (long) 12345.0, (long) 12345.0,
                    (long) 12345.0, (long) 12345.0, (long) 12345.0
                });
        for (int i = 0; i < this.parameters.rngStatusIndex; i++) {
            this.randomStream.resetNextSubstream();
        }

        // At this point files have been successfully loaded
        this.outputStream.println("Simulation has been initialized.");
        this.outputStream.println("The initial state of the random numbers generator is : \n"
                + "  " + Arrays.toString(this.randomStream.getState()));
        this.stopped = false;
        this.currentTime = this.parameters.simBegin;

        observableManager.clear();
        if (this.parameters.observersFile != null) {
            observableManager.setObservers(new FileReader(this.parameters.observersFile), xstream);
        }
        observableManager.initObservers();
        for (ObservablesHandler obs : observableManager.getObservables().values()) {
            for (ObserverListener obsListener : obs.getObservers()) {
                TransientParameters.callAnnotedMethods(obsListener, this);
            }
        }
        this.aquaticWorld.init(this);
    }

    public void run() {
        long nbStepRemaining = (this.parameters.simBegin + this.parameters.simDuration - this.currentTime) / this.parameters.timeStepDuration;
        run(nbStepRemaining);
    }

    public void run(long nbStep) {
        this.stopAtEndOfNextStep = false;
        long endRun = this.currentTime + nbStep * this.parameters.timeStepDuration;
        double beginStep = this.currentTime;
        double ellapsedTime = System.currentTimeMillis();
        while ((this.currentTime < endRun) && (!this.stopAtEndOfNextStep)) {
            //this.outputStream.println("TimeStep : " + this.currentTime);
            this.aquaticWorld.step(this);
            this.currentTime += this.parameters.timeStepDuration;
        }
        ellapsedTime = System.currentTimeMillis() - ellapsedTime;
        if (isFinished()) {
            this.aquaticWorld.end(this);
            this.outputStream.println("\nSimulation finished !");
            stop();
        } else {
            this.aquaticWorld.end(this);
            this.outputStream.println("\nSimulation stopped !");
        }
        this.outputStream.println(" " + (this.currentTime - beginStep) + " time steps computed in " + ellapsedTime / 1000 + " seconds");
        if (this.randomStream instanceof MRG32k3aWithCounter) {
            this.outputStream.println(" number of random numbers generated : " + MRG32k3aWithCounter.getCount());
        }
    }
}
