package fr.cemagref.simaqualife.pilot.rng;

import umontreal.iro.lecuyer.rng.MRG32k3a;

/**
 * <code>MRG32k3aWithCounter</code> Allows to know how many random numbers have been generated.
 */
public class MRG32k3aWithCounter extends MRG32k3a {

    private static long count = 0;
    
    /**
     * @see umontreal.iro.lecuyer.rng.RandomStreamBase#nextDouble()
     */
    @Override
    public double nextDouble() {
        count++;
        return super.nextDouble();
    }

    /**
     * @return Returns how many random numbers have been generated since the initialisation of the stream.
     */
    public static final long getCount() {
        return count;
    }

}
