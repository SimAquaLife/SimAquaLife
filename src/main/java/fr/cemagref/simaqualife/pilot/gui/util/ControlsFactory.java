package fr.cemagref.simaqualife.pilot.gui.util;

import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class ControlsFactory {

	public static void showFileNotFoundExceptionErrorBox(JFrame frame,
			FileNotFoundException exception) {
		JOptionPane.showMessageDialog(frame,
				"Sorry, an error has occured while trying to load a file :\n" + exception.getMessage(),
				"Error", JOptionPane.ERROR_MESSAGE);
	}

	public static void showInternalErrorBox(JFrame frame, Exception exception) {
		JOptionPane
				.showMessageDialog(
						frame,
						"Sorry, an internal error has occurred. Please contact the authors with the following message :\n"
								+ exception.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
	}

	public static JButton makeButton(String imageName, ActionListener listener,
			String toolTipText, String altText) {
		// Look for the image.
		String imgLocation = "toolbarButtonGraphics/" + imageName + ".gif";
		URL imageURL = ControlsFactory.class.getClassLoader().getResource(
				imgLocation);
		// Create and initialize the button.
		JButton button = new JButton();
		button.setToolTipText(toolTipText);
		button.addActionListener(listener);
		if (imageURL != null) {
			// image found
			button.setIcon(new ImageIcon(imageURL, altText));
		} else {
			// no image found
			button.setText(altText);
			System.err.println("Resource not found: " + imgLocation);
		}

		return button;
	}

	public static JMenuItem makeMenuItem(String text, ActionListener listener,
			int mnemonic) {
		JMenuItem menuItem = new JMenuItem(text, mnemonic);
		menuItem.addActionListener(listener);

		return menuItem;
	}

}
