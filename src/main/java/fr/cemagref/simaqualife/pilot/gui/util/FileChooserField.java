package fr.cemagref.simaqualife.pilot.gui.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * <code>FileChooserField</code>
 * @deprecated use OhOUI instead
 */
@SuppressWarnings("serial")
@Deprecated()
public class FileChooserField extends JPanel implements ActionListener {

    private File file = null;
    private JTextField textField;
    private JFileChooser fileChooser;
    
    public FileChooserField(String initValue, JFileChooser fileChooser) {
        super();
        
        this.fileChooser = fileChooser;        
        this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
        
        textField = new JTextField(initValue,20);
        this.add(textField);
        JButton browseButton =  ControlsFactory.makeButton("general/Open16",this,"Browse","...");
        this.add(browseButton);
        
    }

    public FileChooserField(File file) {
        super();
        this.file = file;
        
        this.fileChooser = new JFileChooser();        
        this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
        
        textField = new JTextField(file.getAbsolutePath(),20);
        this.add(textField);
        JButton browseButton =  ControlsFactory.makeButton("general/Open16",this,"Browse","...");
        this.add(browseButton);
        
    }

    public void actionPerformed(ActionEvent e) {
        fileChooser.setCurrentDirectory(new File(textField.getText()));
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            this.file = fileChooser.getSelectedFile();
            textField.setText(file.getAbsolutePath());
        }
    }
    
    public String getValue() {
        return textField.getText();
    }
}
