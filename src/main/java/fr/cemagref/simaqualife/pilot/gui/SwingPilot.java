package fr.cemagref.simaqualife.pilot.gui;

import fr.cemagref.observation.gui.Drawable;
import fr.cemagref.observation.gui.ObserversManagerHandler;
import fr.cemagref.observation.gui.ObserversManagerPanel;
import fr.cemagref.ohoui.core.AbstractOUIPanel;
import fr.cemagref.ohoui.filters.NoTransientField;
import fr.cemagref.ohoui.swing.OhOUI;
import fr.cemagref.simaqualife.extensions.pilot.BatchRunner;
import fr.cemagref.simaqualife.kernel.AquaticWorld;
import fr.cemagref.simaqualife.pilot.Pilot;
import fr.cemagref.simaqualife.pilot.PilotParameters;
import fr.cemagref.simaqualife.pilot.gui.util.ControlsFactory;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.WindowEvent;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.Document;
import net.infonode.docking.DockingWindow;
import net.infonode.docking.RootWindow;
import net.infonode.docking.SplitWindow;
import net.infonode.docking.TabWindow;
import net.infonode.docking.View;
import net.infonode.docking.theme.ShapedGradientDockingTheme;
import net.infonode.docking.util.DockingUtil;
import net.infonode.docking.util.ViewMap;
import net.infonode.util.Direction;

public class SwingPilot extends javax.swing.JFrame implements ObserversManagerHandler {

    private Pilot pilot;
    private RootWindow rootWindow;
    protected AbstractOUIPanel<JComponent> simulationParameterOUIPanel;
    protected AbstractOUIPanel<JComponent> modelOUIPanel;
    private TabWindow mainTabWindow;
    private final static Logger LOGGER = Logger.getLogger(SwingPilot.class.getName());
    private ObserversManagerPanel observersManagerPanel;

    /** Creates new form SwingPilot */
    public SwingPilot(Pilot pilot) throws InfoNodeBugException {
        this.pilot = pilot;
        initComponents();
        String icon = "/logo.png";
        URL iconURL = this.getClass().getResource(icon);
        if (iconURL != null) {
            Image img = new ImageIcon(iconURL).getImage();
            this.setIconImage(img);
        }
        StringBuilder title = new StringBuilder("SimAqualife ");
        try {
            Properties props = new Properties();
            props.load(getClass().getResourceAsStream("application.properties"));
            String projectVersion = props.getProperty("project.version");
            title.append(projectVersion);
            if (projectVersion.contains("SNAPSHOT")) {
                title.append(" - ");
                title.append(props.getProperty("build.timestamp"));
            }
        } catch (IOException ex) {
            LOGGER.log(Level.WARNING, "Application version cannot be loaded", ex);
        }
        this.setTitle(title.toString());
        contentPanel.add(makeRootWindow(), BorderLayout.CENTER);
        this.setPreferredSize(new Dimension(800,600));

        this.pack();
        this.setVisible(true);
    }

    protected RootWindow makeRootWindow() throws InfoNodeBugException {
        ViewMap viewMap = new ViewMap();
        simulationParameterOUIPanel = new OhOUI().getOUIPanel(pilot.getParameters(), new NoTransientField());
        // Sim parameters
        View simParametersView = new View("Simulation parameters", null, new JScrollPane(simulationParameterOUIPanel.getPanel()));
        simParametersView.getWindowProperties().setCloseEnabled(false);
        viewMap.addView(1, simParametersView);
        observersManagerPanel = new ObserversManagerPanel(pilot.getObservableManager(), this);

        // Observers
        View observersView = new View("Observers", null, observersManagerPanel);
        observersView.getWindowProperties().setCloseEnabled(false);
        viewMap.addView(2, observersView);

        JTextArea consoleTextArea = new JTextArea();
        System.setOut(new PrintStream(new ConsoleOutputStream(consoleTextArea, System.out), true));
        //System.setErr( new PrintStream( new ConsoleOutputStream (consoleTextArea, null), true));

        View consoleView = new View("Console", null, new JScrollPane(consoleTextArea));
        consoleView.getWindowProperties().setCloseEnabled(false);
        viewMap.addView(2, observersView);

        modelOUIPanel = new OhOUI().getOUIPanel(pilot.getAquaticWorld(), new NoTransientField());
        View modelEditorView = new View("Model Editor", null, new JScrollPane(modelOUIPanel.getPanel()));
        modelEditorView.getWindowProperties().setCloseEnabled(false);
        viewMap.addView(0, modelEditorView);

        rootWindow = DockingUtil.createRootWindow(viewMap, true);
        rootWindow.getWindowBar(Direction.DOWN).setEnabled(true);
        try {
            mainTabWindow = new TabWindow(new DockingWindow[]{modelEditorView, simParametersView, observersView});
        } catch (Exception e) {
            throw new InfoNodeBugException(e);
        }
        mainTabWindow.getWindowProperties().setCloseEnabled(false);
        rootWindow.setWindow(new SplitWindow(false, 0.7f, mainTabWindow, consoleView));

        rootWindow.getRootWindowProperties().addSuperObject(new ShapedGradientDockingTheme().getRootWindowProperties());

        // Show modelEditorView first
        modelEditorView.restoreFocus();
        return rootWindow;

    }

    private void loadInitPilot() {
        // validating form edition
        pilot.setParameters((PilotParameters) simulationParameterOUIPanel.getObject());
        pilot.setAquaticWorld((AquaticWorld) modelOUIPanel.getObject());
        // load model
        try {
            pilot.load();
            observersManagerPanel.reloadObservers(pilot.getObservableManager());
        } catch (FileNotFoundException e1) {
            ControlsFactory.showFileNotFoundExceptionErrorBox(SwingPilot.this, e1);
        } catch (Exception e) {
            ControlsFactory.showInternalErrorBox(SwingPilot.this, e);
            e.printStackTrace();
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        timeTextField = new javax.swing.JTextField();
        jToolBar1 = new javax.swing.JToolBar();
        initButton = new javax.swing.JButton();
        runButton = new javax.swing.JButton();
        stepButton = new javax.swing.JButton();
        pauseButton = new javax.swing.JButton();
        stopButton = new javax.swing.JButton();
        contentPanel = new javax.swing.JPanel();

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setText("Time:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(timeTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE)
                .addComponent(timeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SimAquaLife");

        jToolBar1.setRollover(true);

        initButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Refresh16.gif"))); // NOI18N
        initButton.setToolTipText("Initialize the simulation");
        initButton.setFocusable(false);
        initButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        initButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        initButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                init(evt);
            }
        });
        jToolBar1.add(initButton);
        initButton.getAccessibleContext().setAccessibleName("Init");

        runButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/media/Play16.gif"))); // NOI18N
        runButton.setToolTipText("Run the simulation");
        runButton.setFocusable(false);
        runButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        runButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        runButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                run(evt);
            }
        });
        jToolBar1.add(runButton);
        runButton.getAccessibleContext().setAccessibleName("Play");

        stepButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/media/StepForward16.gif"))); // NOI18N
        stepButton.setToolTipText("Step forward the simulation");
        stepButton.setFocusable(false);
        stepButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        stepButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        stepButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                step(evt);
            }
        });
        jToolBar1.add(stepButton);
        stepButton.getAccessibleContext().setAccessibleName("Step");

        pauseButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/media/Pause16.gif"))); // NOI18N
        pauseButton.setToolTipText("Pause the simulation");
        pauseButton.setEnabled(false);
        pauseButton.setFocusable(false);
        pauseButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pauseButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pauseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pause(evt);
            }
        });
        jToolBar1.add(pauseButton);
        pauseButton.getAccessibleContext().setAccessibleName("Pause");

        stopButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/media/Stop16.gif"))); // NOI18N
        stopButton.setToolTipText("Stop the simulation");
        stopButton.setEnabled(false);
        stopButton.setFocusable(false);
        stopButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        stopButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stop(evt);
            }
        });
        jToolBar1.add(stopButton);
        stopButton.getAccessibleContext().setAccessibleName("Stop");

        contentPanel.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
            .addComponent(contentPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(contentPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void init(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_init
        loadInitPilot();
        runButton.setEnabled(true);
        stepButton.setEnabled(true);
    }//GEN-LAST:event_init

    private void run(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_run
        initButton.setEnabled(false);
        runButton.setEnabled(false);
        stepButton.setEnabled(false);
        pauseButton.setEnabled(true);
        stopButton.setEnabled(true);
        if (pilot.isStopped()) {
            loadInitPilot();
        }
        // run the simulation in a independent thread
        new Thread() {

            public void run() {
                pilot.run();
            }
        }.start();
        if (pilot.isFinished()) {
            initButton.setEnabled(true);
            runButton.setEnabled(false);
            stepButton.setEnabled(false);
            pauseButton.setEnabled(false);
            stopButton.setEnabled(false);
        }       
}//GEN-LAST:event_run

    private void pause(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pause
        initButton.setEnabled(false);
        runButton.setEnabled(true);
        stepButton.setEnabled(true);
        pauseButton.setEnabled(false);
        stopButton.setEnabled(true);
        pilot.pause();
    }//GEN-LAST:event_pause

    private void stop(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stop
        if (JOptionPane.showConfirmDialog(SwingPilot.this,
                "You're about to end the current simulation.\nIf you want to pause the simulation,\nthen click button \"Pause\" !\nAre you sure to stop ?",
                "Confirmation", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE) == 0) {
            initButton.setEnabled(true);
            pauseButton.setEnabled(false);
            runButton.setEnabled(true);
            stepButton.setEnabled(true);
            stopButton.setEnabled(false);
            pilot.stop();
        }
    }//GEN-LAST:event_stop

    private void step(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_step
        initButton.setEnabled(false);
        runButton.setEnabled(true);
        stepButton.setEnabled(true);
        pauseButton.setEnabled(false);
        stopButton.setEnabled(true);
        if (pilot.isStopped()) {
            loadInitPilot();
        }
        pilot.run(1);            
    }//GEN-LAST:event_step

    ////////////////////////////////////////////////////////////
    //
    // MAIN
    public static void main(String[] args) throws IOException {
        Pilot pilot = new Pilot();
        if (args.length > 0) {
            new BatchRunner(pilot).parseArgs(args, false, false, false);
        }
        pilot.init();
        // Hugly patch for a bug of docking framework of InfoNode
        int tries = 5;
        while (tries > 0) {
            try {
                new SwingPilot(pilot);
                tries = -1;
            } catch (InfoNodeBugException e) {
                tries--;
                if (tries == 0) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void windowClosing(WindowEvent e) {
        pilot.stop();
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel contentPanel;
    private javax.swing.JButton initButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton pauseButton;
    private javax.swing.JButton runButton;
    private javax.swing.JButton stepButton;
    private javax.swing.JButton stopButton;
    private javax.swing.JTextField timeTextField;
    // End of variables declaration//GEN-END:variables
    public JFrame getHandlingFrame() {
        return this;
    }

    public void showDrawable(Drawable drawable) {
        mainTabWindow.addTab(new View(drawable.getTitle(), null, drawable.getDisplay()));
    }

    public static class InfoNodeBugException extends Exception {

        public InfoNodeBugException(Exception e) {
            super(e);
        }
    }

    public class ConsoleOutputStream extends OutputStream {

        private final JTextArea textArea;
        private Document document = null;
        private ByteArrayOutputStream outputStream = new ByteArrayOutputStream(256);
        private PrintStream ps = null;

        public ConsoleOutputStream(JTextArea textArea, PrintStream ps) {
            this.textArea = textArea;
            this.document = textArea.getDocument();
            this.ps = ps;
            new Thread() {

                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ConsoleOutputStream.this.textArea.setCaretPosition(ConsoleOutputStream.this.textArea.getDocument().getLength());
                    }
                }
            }.start();
        }

        public void write(int b) {
            outputStream.write(b);
        }

        public void flush() throws IOException {
            super.flush();

            try {
                document.insertString(document.getLength(), new String(outputStream.toByteArray()), null);
                if (ps != null) {
                    ps.write(outputStream.toByteArray());
                }
                outputStream.reset();
            } catch (Exception e) {
            }
        }
    }
}
