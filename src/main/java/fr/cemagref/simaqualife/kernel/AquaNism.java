package fr.cemagref.simaqualife.kernel;

import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.simaqualife.kernel.spatial.Environment;
import fr.cemagref.simaqualife.kernel.spatial.Position;
import fr.cemagref.simaqualife.pilot.Pilot;

abstract public class AquaNism<P extends Position, E extends Environment> {

  /**
   * <code>position</code> is the current position of the aquaNism.
   */
  @Observable(description = "Position")
  protected P position = null;
  protected static transient ObservablesHandler cobservable;

  /**
   * @param position
   */
  public AquaNism(Pilot pilot, P position) {
    this.position = position;
    if (cobservable == null) {
      cobservable = pilot.addObservable(this.getClass());
    }
  }

  public E getEnvironment(Pilot pilot) {
    // TODO : voir si on peut pas mieux faire que ce cast
    return (E) pilot.getAquaticWorld().getEnvironment();
  }

  /**
   * @return Returns the position.
   */
  public P getPosition() {
    return position;
  }

  /**
   * Moves an aquanism to a new position. Environnement is automatically updated.
   *
   * @param destination
   */
  public <ANG extends AquaNismsGroup<?, E>> void moveTo(Pilot pilot, P destination, ANG group) {
    if (this.position != destination) {
      this.getEnvironment(pilot).moveAquaNism(this, group, (Position) destination);
      this.position = destination;
      if (cobservable != null) {
        cobservable.fireChanges(this, pilot.getCurrentTime());
      }
    }
  }

  @Override
  public String toString() {
    return position.toString();
  }
}
