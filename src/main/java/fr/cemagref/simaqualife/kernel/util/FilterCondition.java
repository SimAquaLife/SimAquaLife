package fr.cemagref.simaqualife.kernel.util;

/**
 * TODO Description <code>FilterCondition</code>
 */
public interface FilterCondition<T> {
    /**
     * TODO Description of <code>isSatisfiedBy</code>
     *
     */
    public boolean isSatisfiedBy(T object);
}
