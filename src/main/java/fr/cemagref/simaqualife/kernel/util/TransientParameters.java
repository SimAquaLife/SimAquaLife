package fr.cemagref.simaqualife.kernel.util;

import fr.cemagref.simaqualife.pilot.Pilot;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TransientParameters {
    
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public static @interface InitTransientParameters {
    }
    
    public static List<Method> fetchInitMethods(Class type) {
        List<Method> methods = new ArrayList<Method>();
        Class inheritanceNavigator = type;
        while (inheritanceNavigator != null) {
            for (Method method : inheritanceNavigator.getDeclaredMethods()) {
                if (method.isAnnotationPresent(InitTransientParameters.class)) {
                    methods.add(method);
                }
            }
            inheritanceNavigator = inheritanceNavigator.getSuperclass();
        }
        return methods;
    }
    
    public static void callAnnotedMethods(Object object, Pilot pilot, Object arg) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        for (Method method : TransientParameters.fetchInitMethods(object.getClass())) {
            method.setAccessible(true);
            try {
                // TODO test if the first argument is as excepted of class Pilot
                if (arg != null && method.getParameterTypes().length > 1) {
                    method.invoke(object, pilot, arg);
                } else {
                    method.invoke(object, pilot);
                }
            } catch (IllegalArgumentException e) {
                Logger.getLogger(TransientParameters.class.getName()).log(Level.SEVERE, "Error while trying to call method ''{0}'' in class ''{1}''.",
                        new Object[]{method.getName(), object.getClass().getName()});
                throw e;
            }
        }
    }
    
    public static void callAnnotedMethods(Object object, Pilot pilot) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        callAnnotedMethods(object, pilot, null);
    }
    
}
