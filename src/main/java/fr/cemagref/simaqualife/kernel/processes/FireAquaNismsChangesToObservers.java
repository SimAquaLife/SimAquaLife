package fr.cemagref.simaqualife.kernel.processes;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class FireAquaNismsChangesToObservers extends LoopAquaNismsGroupProcess<AquaNism, AquaNismsGroup<AquaNism, ?>> {

    private transient ObservablesHandler cObservable;

    @Override
    protected void doProcess(AquaNism aquaNism, AquaNismsGroup group) {
        if (cObservable == null) {
            Class aquanismClass;
            Type aquanismType = ((ParameterizedType) group.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            if (aquanismType instanceof ParameterizedType) // group is parametrized with an abstract type of AquaNism
            {
                aquanismClass = (Class) ((ParameterizedType) aquanismType).getRawType();
            } else {
                aquanismClass = (Class) aquanismType;
            }
            cObservable = group.getPilot().addObservable(aquanismClass);
        } else {
            cObservable.fireChanges(aquaNism, group.getPilot().getCurrentTime());
        }
    }
}
