package fr.cemagref.simaqualife.kernel.processes;

import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess.Synchronisation;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


/**
 * <code>LoopAquaNismsGroupProcess</code> allow to launch a process on each
 * aquaNism of a aquaNismsGroup.
 */

public abstract class LoopAquaNismsGroupProcess<AN extends AquaNism, ANG extends AquaNismsGroup<AN, ?>>
		extends AquaNismsGroupProcess<AN,ANG> {

	/**
	 * This annotations intends to specify which methods should be called at the
	 * begin of the process loop.
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface AtBegin {
	}

	/**
	 * This annotations intends to specify which methods should be called at the
	 * end of the process loop.
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface AtEnd {
	}

	/**
	 * Iterator of the loop. May be null if no one iterator is used.
	 */
	private transient Iterator<AN> iterator = null;

	transient protected boolean sorted = false;

	transient protected Comparator<AN> comparator = null;

	/**
	 * Cache of methods that should be called at the begin of the process loop.
	 */
	transient protected List<Method> methodsAtBegin;

	/**
	 * Cache of methods that should be called at the end of the process loop.
	 */
	transient protected List<Method> methodsAtEnd;

	/**
	 * Fetch the methods that are annoted as AtBegin or (not exclusive) AtEnd.
	 * These methods are stored in two internal lists for better performance at
	 * call.
	 */
	@InitTransientParameters
	public void initAtBeginAndAtEndMethodsLists(Pilot pilot) {
		methodsAtBegin = new ArrayList<Method>();
		methodsAtEnd = new ArrayList<Method>();
		Class inheritanceNavigator = this.getClass();
		while (inheritanceNavigator != null) {
			for (Method method : inheritanceNavigator.getDeclaredMethods()) {
				if (method.isAnnotationPresent(AtBegin.class))
					methodsAtBegin.add(method);
				if (method.isAnnotationPresent(AtEnd.class))
					methodsAtEnd.add(method);
			}
			inheritanceNavigator = inheritanceNavigator.getSuperclass();
		}

	}

	/**
	 * Run the methods annoted as AtBegin.
	 * 
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * 
	 * @see {@link java.lang.reflect.Method#invoke(Object, Object[])}
	 */
	public void runAtBeginMethods() throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		for (Method method : methodsAtBegin)
			method.invoke(this);
	}

	/**
	 * Run the methods annoted as AtEnd.
	 * 
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * 
	 * @see {@link java.lang.reflect.Method#invoke(Object, Object[])}
	 */
	public void runAtEndMethods() throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		for (Method method : methodsAtEnd)
			method.invoke(this);
	}

	@Override
	public void interAquanism(int atIndex, ANG group) {
		if (synchronisationMode == Synchronisation.ASYNCHRONOUS) {
			group.getAquaNismsList().remove(atIndex);
			if (this.indix > atIndex)
				this.indix--;
			this.listSize--;
		} else {
			this.aquanismsToInter.add(atIndex);
		}
	}

	/**
	 * @see fr.cemagref.simaqualife.kernel.processes.Process#doProcess(null)
	 */
	public void doProcess(ANG group) {
		List<AN> list;
		list = group.getAquaNismsList();
		if (sorted) {
			List<AN> sortedList = new ArrayList<AN>(list);
			Collections.sort(sortedList, comparator);
			list = sortedList;
		}
		if (synchronisationMode == Synchronisation.ASYNCHRONOUS) {
			this.indix = 0;
			this.listSize = list.size();
			while (this.indix < list.size()) {
				this.doProcess(list.get(this.indix), group);
				this.indix++;
			}
			this.listSize = -1;
			this.indix = -1;
		} else {
			for (this.iterator = list.iterator(); this.iterator.hasNext();) {
				this.doProcess(iterator.next(), group);
			}
			this.iterator = null;
			if (synchronisationMode == Synchronisation.LOCAL_SYNCHRONOUS) {
				this.commitPendingActions(group);
			}
		}
	}

	/**
	 * <code>doProcess</code> is the content of the individual process called
	 * for each aquaNism
	 * 
	 */
	protected abstract void doProcess(AN aquanism, ANG group);

	// TODO : The aquaNismsGroup loop can be shuffled with an uniform RNG
	/*
	 * private <T> Set<T> shuffle(Set<T> list) { Set<T> result=new HashSet<T>(list);
	 * for (int i = result.size()-1; i > 0; i--) { swap(result, i,
	 * shuffler.nextIntFromTo(0,i)); } return result; }
	 * 
	 * private <T> void swap(Set<T> list, int i, int j) { private <T> void
	 * swap(List<T> list, int i, int j) { // fix this T tmp = list.get(i);
	 * list.set(i, list.get(j)); list.set(j, tmp); }
	 */
}
