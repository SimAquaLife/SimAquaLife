package fr.cemagref.simaqualife.kernel.processes;

import fr.cemagref.observation.kernel.ObservablesHandler;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * TODO Description
 * <code>AquaNismsGroupProcess</code>
 */
public abstract class AquaNismsGroupProcess<AN extends AquaNism, ANG extends AquaNismsGroup<AN, ?>> implements Process<ANG> {

    private transient ANG group = null;
    private ObservablesHandler observable = null;
    protected transient Pilot pilot;
    /**
     * Synchronisation mode of the execution of the process through the group of
     * aquanisms.
     *
     * <ul> <li><em>LOCAL_SYNCHRONOUS : </em> Execution in two passes. First,
     * the aquanisms are activated, and the body of the doProcess function is
     * executed. But the resulting actions are enqueued and executed at the end
     * of the loop of the current process.</li> <li><em>GROUP_SYNCHRONOUS :
     * </em> Execution in two passes. First, processes of the current group are
     * executed, but the resulting actions are enqueued and executed at the end
     * of the loop of the processes of the current group.</li>
     * <li><em>SYNCHRONOUS : </em> Execution in two passes. First, the aquanisms
     * are activated, and the body of the doProcess function is executed. But
     * the resulting actions are enqueued and executed at the end of the
     * time-step.</li> <li><em>ASYNCHRONOUS : </em> Execution in one pass. The
     * activation of the aquanisms and the action resulting of the process are
     * achieved at the same time, for each aquanism in the loop.</li> </ul>
     */
    // TODO link this mode to associated method to avoid test on the actual mode
    public enum Synchronisation {

        LOCAL_SYNCHRONOUS, GROUP_SYNCHRONOUS, SYNCHRONOUS, ASYNCHRONOUS;
    }
    protected Synchronisation synchronisationMode = Synchronisation.ASYNCHRONOUS;
    /**
     * The indix of the current element in the loop. This indix is used when the
     * iterator is unused. Otherwise, indix value is -1.
     */
    protected transient int indix = -1;
    /**
     * The number of elements of the loop. This number is used when the iterator
     * is unused. Otherwise, number value is -1.
     */
    protected transient int listSize = -1;
    protected transient Set<Integer> aquanismsToInter = null;
    protected transient List<AN> aquanismsToAdd = null;

    @InitTransientParameters
    public void initTransientParameters(Pilot pilot) {
        this.pilot = pilot;
        // build an inverse sorted set
        this.aquanismsToInter = new TreeSet<Integer>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });
        this.aquanismsToAdd = new ArrayList<AN>();
    }

    public ANG getGroup() {
        return group;
    }

    public void setGroup(ANG group) {
        this.group = group;
    }

    public Synchronisation getSynchronisationMode() {
        return synchronisationMode;
    }

    public void commitPendingActions(ANG group) {
        for (int i : this.aquanismsToInter) {
            group.getAquaNismsList().remove(i);
        }
        this.aquanismsToInter.clear();
        for (AN aquanism : this.aquanismsToAdd) {
            group.getAquaNismsList().add(aquanism);
        }
        this.aquanismsToAdd.clear();
    }

    public void addAquanism(AN aquanism, ANG group) {
        if (synchronisationMode == Synchronisation.ASYNCHRONOUS) {
            group.getAquaNismsList().add(aquanism);
        } else {
            this.aquanismsToAdd.add(aquanism);
        }
    }

    public void interAquanism(AquaNism aquanism) {
        int index = this.group.getAquaNismsList().indexOf(aquanism);
        if (index >= 0) {
            this.interAquanism(index, this.group);
        } else {
            // TODO what to do with sync mode ?
            for (AquaNismsGroup aGroup : this.group.getWorld().getAquaNismsGroupsList()) {
                if (aGroup.getAquaNismsList().remove(aquanism)) {
                    return;
                }
            }
        }
    }

    public void interAquanism(int atIndex, ANG group) {
        if (synchronisationMode == Synchronisation.ASYNCHRONOUS) {
            group.getAquaNismsList().remove(atIndex);
            if (this.indix > atIndex) {
                this.indix--;
            }
            this.listSize--;
        } else {
            this.aquanismsToInter.add(atIndex);
        }
    }

    /**
     * Notify all connected observers to your process that a change may have occured.
     * You can call this method to force the connected observers to refresh them.
     */
    protected void fireChangesToObservers() {
        if (observable==null) {
            observable = pilot.addObservable(this.getClass());
        }
        observable.fireChanges(this, pilot.getCurrentTime());
    }
}
