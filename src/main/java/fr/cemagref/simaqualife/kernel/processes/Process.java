package fr.cemagref.simaqualife.kernel.processes;

/**
 * TODO Description <code>Process</code>
 */
public interface Process<T> {

    public void doProcess (T object);
}
