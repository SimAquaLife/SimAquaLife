package fr.cemagref.simaqualife.kernel;

import java.util.List;

import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 * <code>Processes</code> encapsulates two lists of processes. The first is
 * executed at the beginning and is dedicated to the initialisation (if needed).
 * The processes in the second list are executed at each time step.
 */
public class Processes {

	@Description(name = "Initialisation Processes", tooltip = "These processes will only be executed at the beginning of the simulation.")
	private List<AquaNismsGroupProcess> processesAtBegin;

	@Description(name = "Simulation Processes", tooltip = "These processes will be executed at each timestep of the simulation.")
	private List<AquaNismsGroupProcess> processesEachStep;

	@Description(name = "Closure Processes", tooltip = "These processes will only be executed at the end of the simulation.")
	private List<AquaNismsGroupProcess> processesAtEnd;

	public Processes(List<AquaNismsGroupProcess> processesAtBegin,
			List<AquaNismsGroupProcess> processesEachStep,
			List<AquaNismsGroupProcess> processesAtEnd) {
		this.processesAtBegin = processesAtBegin;
		this.processesEachStep = processesEachStep;
		this.processesAtEnd = processesAtEnd;
	}

	public List<AquaNismsGroupProcess> getProcessesAtBegin() {
		return processesAtBegin;
	}

	public List<AquaNismsGroupProcess> getProcessesAtEnd() {
		return processesAtEnd;
	}

	public List<AquaNismsGroupProcess> getProcessesEachStep() {
		return processesEachStep;
	}
	
}