package fr.cemagref.simaqualife.kernel.spatial;

import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;

import java.util.Collection;
import java.util.HashSet;

public class Cell<AN extends AquaNism> implements Position {

    protected transient Collection<AN> aquaNisms;
    
    public Cell() {
        this.initTransientParameters();
    }
    
    public void initTransientParameters() {
        aquaNisms = new HashSet<AN>();
    }

    public Collection<AN> getAquaNisms() {
        return aquaNisms;
    }
    public void addAquaNism(AN aquaNism, AquaNismsGroup group) {
        this.aquaNisms.add(aquaNism);
    }
    public void removeAquaNism(AN aquaNism, AquaNismsGroup group) {
        this.aquaNisms.remove(aquaNism);
    }

}
