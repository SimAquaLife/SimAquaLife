package fr.cemagref.simaqualife.kernel.spatial;

import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.util.FilterCondition;
import fr.cemagref.simaqualife.kernel.util.TransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;
import java.util.List;

public abstract class Environment<P extends Position, AN extends AquaNism<P, ?>> {

    protected Pilot pilot;

    public Pilot getPilot() {
        return pilot;
    }
    
    @TransientParameters.InitTransientParameters
    protected final void initTransientParametersPilot(Pilot pilot) {
        this.pilot = pilot;
    }

    public abstract void addAquaNism(AN aquaNism, AquaNismsGroup group);

    public abstract void removeAquaNism(AN aquaNism, AquaNismsGroup group);

    /**
     * <code>moveAquaNism</code> allow to move a aquaNism from its current
     * location to a new location.
     *
     * @param aquaNism The aquaNism to move
     * @param group
     * @param destination The final position of move
     */
    public abstract void moveAquaNism(final AN aquaNism, AquaNismsGroup group, P destination);

    /**
     * <code>getNeighbours</code>
     *
     * @param position The initial position
     * @return The list of neighboors
     */
    public abstract List<P> getNeighbours(P position);

    /**
     * <code>getNeighbours</code>
     *
     * @param position
     * @param filter
     * @return
     */
    public List<P> getNeighbours(P position, FilterCondition<P> filter) {
        List<P> result = getNeighbours(position);
        for (P element : result) {
            if (filter.isSatisfiedBy(element)) {
                result.remove(element);
            }
        }
        return result;
    }
}
