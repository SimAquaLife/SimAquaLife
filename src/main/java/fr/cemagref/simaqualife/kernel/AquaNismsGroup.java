package fr.cemagref.simaqualife.kernel;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.processes.LoopAquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess.Synchronisation;
import fr.cemagref.simaqualife.kernel.spatial.Environment;
import fr.cemagref.simaqualife.kernel.util.TransientParameters;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service=AquaNismsGroup.class)
public class AquaNismsGroup<AN extends AquaNism, E extends Environment> {

    @Description(name = "Processes for this group", tooltip = "")
    private Processes processes;
    private transient List<LoopAquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>>> groupSynchronousProcesses;
    private transient List<LoopAquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>>> synchronousProcesses;
    private transient List<AN> aquaNismsList = new LinkedList<AN>();
    private transient List<AN> cemetery = new ArrayList<AN>();
    private boolean useCemetery = true;
    protected transient Pilot pilot;
    protected transient E environment;
    protected transient AquaticWorld<E, AN, AquaNismsGroup<AN, E>> world;
    protected transient ObservablesHandler cobservable;

    public Pilot getPilot() {
        return pilot;
    }
    
    /**
     * @return Returns the environment.
     */
    public E getEnvironment() {
        return environment;
    }

    public void setEnvironment(E environment) {
        this.environment = environment;
    }

    public AquaticWorld<E, AN, AquaNismsGroup<AN, E>> getWorld() {
        return world;
    }

    public AquaNismsGroup() {
    }
    
    /**
     * @param pilot
     * @param environment The environment of this aquaNismsGroup
     * @param processes The two lists of processes
     */
    public AquaNismsGroup(final Pilot pilot, final E environment, final Processes processes) {
        this.pilot = pilot;
        this.environment = environment;
        this.processes = processes;
    }

    @InitTransientParameters
    public void initTransientParameters(Pilot pilot) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        this.pilot = pilot;
        aquaNismsList = new LinkedList<AN>();
        cemetery = new ArrayList<AN>();
        for (AquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>> process : processes.getProcessesAtBegin()) {
            process.setGroup(this);
            // Processes are allowed to declared a method with the group as argument
            TransientParameters.callAnnotedMethods(process, pilot, this);
        }
        groupSynchronousProcesses = new ArrayList<LoopAquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>>>();
        synchronousProcesses = new ArrayList<LoopAquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>>>();
        
        if (processes.getProcessesEachStep() != null) {
            for (AquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>> process : processes.getProcessesEachStep()) {
                process.setGroup(this);
                TransientParameters.callAnnotedMethods(process, pilot);
                if (process instanceof LoopAquaNismsGroupProcess) {
                    if (((LoopAquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>>) process).getSynchronisationMode() == Synchronisation.GROUP_SYNCHRONOUS) {
                        groupSynchronousProcesses.add((LoopAquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>>) process);
                    } else if (((LoopAquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>>) process).getSynchronisationMode() == Synchronisation.SYNCHRONOUS) {
                        synchronousProcesses.add((LoopAquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>>) process);
                    }
                }
            }
        }
        if (cobservable == null) {
            cobservable = pilot.addObservable(this.getClass());
        }
    }

    /**
     * Add an object of type AquaNism to the List aquaNismsList
     * 
     * @return void
     */
    public void addAquaNism(final AN aquanism) {
        aquaNismsList.add(aquanism);
        if (environment != null) {
            environment.addAquaNism(aquanism, this);
        }
    }

    /**
     * remove an object of type AquaNism to the List aquaNismsList
     * 
     * @return void
     */
    public void removeAquaNism(final AN aquanism) {
        aquaNismsList.remove(aquanism);
        if (environment != null) {
            environment.removeAquaNism(aquanism, this);
        }
    }

    /**
     * @return Returns the list of alive aquaNisms of this aquaNismsGroup.
     */
    public final List<AN> getAquaNismsList() {
        return aquaNismsList;
    }
    
     /**
      * A convenient method to access an aquanism in a group
     * @return Returns the aquanism of this aquaNismsGroup if it exist
     */
    // FIXME What is this method for?
       public AN getAquaNism(AN aquanism){
        int index=aquaNismsList.indexOf(aquanism);
        return (index>=0)? aquaNismsList.get(index):null;
    }


    /**
     * @return Returns the list of dead aquaNisms of this aquaNismsGroup.
     */
    public final List<AN> getCemetery() {
        return cemetery;
    }

    public void setUseCemetery(boolean useCemetery) {
        this.useCemetery = useCemetery;
    }

    /**
     * <code>inter</code> moves a aquaNism to the cemetery.
     *
     * @param aquaNism
     */
    // TODO DOC expliquer sur quoi ça agit
    // TODO DOC monter un exemple de loop avec itérateur
    public final void inter(Iterator<AN> iterator, AN aquaNism) {
        if (environment != null) {
            this.environment.removeAquaNism(aquaNism, this);
        }
        // this instructions causes a ConcurrentModificationException in a LoopAquaNismsGroupProcess
        //this.removeAquaNism((AN)aquaNism);
        // this could be a solution :
        iterator.remove();
        if (this.useCemetery) {
            this.cemetery.add((AN) aquaNism);
        }
    }

    public void atBeginning() {
        if (processes.getProcessesAtBegin() != null) {
            for (AquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>> process : processes.getProcessesAtBegin()) {
                process.doProcess(this);
            }
            for (LoopAquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>> process : groupSynchronousProcesses) {
                process.commitPendingActions(this);
            }
        }
        if (cobservable != null) {
            cobservable.fireChanges(this, pilot.getCurrentTime() - 1);
        }
    }

    public void step() {
        for (AquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>> process : processes.getProcessesEachStep()) {
            process.doProcess(this);
        }
        for (LoopAquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>> process : groupSynchronousProcesses) {
            process.commitPendingActions(this);
        }
        if (cobservable != null) {
           cobservable.fireChanges(this, pilot.getCurrentTime());
        }
    }

    public void atEnd() {
        if (processes.getProcessesAtEnd() != null) {
            for (AquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>> process : processes.getProcessesAtEnd()) {
                process.doProcess(this);
            }
        }
    }

    public void commitPendingActions() {
        for (LoopAquaNismsGroupProcess<AN, AquaNismsGroup<AN, E>> process : synchronousProcesses) {
            process.commitPendingActions(this);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer(this.getClass().getSimpleName());
        if (aquaNismsList == null) {
            buffer.append("(empty)");
        } else {
            buffer.append("(");
            buffer.append(aquaNismsList.size());
            buffer.append(")");
        }
        return buffer.toString();
    }
}

