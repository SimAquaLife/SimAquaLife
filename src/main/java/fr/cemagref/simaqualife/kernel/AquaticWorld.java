package fr.cemagref.simaqualife.kernel;

import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObservableManager;
import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.ohoui.annotations.XStreamable;
import fr.cemagref.simaqualife.kernel.spatial.Environment;
import fr.cemagref.simaqualife.kernel.util.TransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class AquaticWorld<E extends Environment, AN extends AquaNism, ANG extends AquaNismsGroup<AN, E>> {

    @XStreamable(load = true, save = true)
    @Description(name = "Groups of individuals", tooltip = "")
    List<ANG> aquaNismsGroupsList;
    @XStreamable(load = true, save = true)
    @Description(name = "Model environment", tooltip = "")
    protected E environment;
    protected transient ObservablesHandler aquaticWorldCobservable;
    protected transient ObservablesHandler environmentObservable;

    public void setEnvironment(Reader envParameters, XStream xstream) {
        this.environment = (E) xstream.fromXML(envParameters);
    }

    public void setEnvironment(Reader envParameters) {
        setEnvironment(envParameters, new XStream(new DomDriver()));
    }

    public void setGroupsParameters(Reader groupsParameters) {
        aquaNismsGroupsList = (List<ANG>) new XStream(new DomDriver()).fromXML(groupsParameters);
    }

    public void setGroupsParameters(Reader groupsParameters, XStream xstream) {
        aquaNismsGroupsList = (List<ANG>) xstream.fromXML(groupsParameters);
    }

    /**
     * @return Returns the environment.
     */
    public E getEnvironment() {
        return environment;
    }

    public boolean removeAquaNismsGroup(final ANG arg0) {
        return aquaNismsGroupsList.remove(arg0);
    }

    public void init(Pilot pilot) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        aquaticWorldCobservable = pilot.addObservable(AquaticWorld.class);
        environmentObservable = pilot.addObservable(pilot.getEnvironmentClass());
        if (this.environment != null) {
            TransientParameters.callAnnotedMethods(environment, pilot);
        }
        for (ANG group : aquaNismsGroupsList) {
            group.setEnvironment(this.environment);
            TransientParameters.callAnnotedMethods(group, pilot);
        }
        for (ANG group : aquaNismsGroupsList) {
            group.atBeginning();
        }
        aquaticWorldCobservable.fireChanges(this, pilot.getCurrentTime());
        environmentObservable.fireChanges(environment, pilot.getCurrentTime());
    }

    public void step(Pilot pilot) {
        for (ANG group : aquaNismsGroupsList) {
            group.step();
        }
        for (ANG group : aquaNismsGroupsList) {
            group.commitPendingActions();
        }
        aquaticWorldCobservable.fireChanges(this, pilot.getCurrentTime());
        environmentObservable.fireChanges(environment, pilot.getCurrentTime());
    }

    public void end(Pilot pilot) {
        for (ANG group : aquaNismsGroupsList) {
            group.atEnd();
        }
        aquaticWorldCobservable.fireChanges(this, pilot.getCurrentTime());
        environmentObservable.fireChanges(environment, pilot.getCurrentTime());
        pilot.closeObservers();
    }

    public List<ANG> getAquaNismsGroupsList() {
        return aquaNismsGroupsList;
    }
}
