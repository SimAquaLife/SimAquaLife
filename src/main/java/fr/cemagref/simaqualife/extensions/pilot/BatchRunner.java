package fr.cemagref.simaqualife.extensions.pilot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import fr.cemagref.simaqualife.pilot.Pilot;

public class BatchRunner {

    private final Pilot pilot;

    public BatchRunner(Pilot pilot) {
        this.pilot = pilot;
    }
    
    public void parseArgs(String[] args, boolean envFileRequired, boolean groupsFileRequired, boolean observersFileRequired) throws IOException {

        Option optionDebug = new Option("d", "Set on the debug mode.");

        Option optionQuiet = new Option("q", "Set on the quiet mode (no message in the console).");

        Option optionEnvFiles = new Option("env", "parameters for the initialization of the environment.");
        optionEnvFiles.setArgs(1);
        optionEnvFiles.setArgName("filename");
        optionEnvFiles.setRequired(envFileRequired);

        Option optionGroupsFiles = new Option("groups",
                "parameters for the initialization of the list of aquaNismsGroups");
        optionGroupsFiles.setArgs(1);
        optionGroupsFiles.setArgName("filename");
        optionGroupsFiles.setRequired(groupsFileRequired);

        Option optionObserversFiles = new Option("observers",
                "parameters for the initialization of the list of observers");
        optionObserversFiles.setArgs(1);
        optionObserversFiles.setArgName("filename");
        optionObserversFiles.setRequired(observersFileRequired);

        Option optionSimBegin = new Option("simBegin", "The time when the simulation starts (default " + pilot.getSimBegin() + ")");
        optionSimBegin.setArgs(1);
        optionSimBegin.setArgName("value");
        optionSimBegin.setRequired(false);
        Option optionSimDuration = new Option("simDuration", "The duration of the simulation");
        optionSimDuration.setArgs(1);
        optionSimDuration.setArgName("value");
        optionSimDuration.setRequired(true);
        Option optionSimTimeStepDuration = new Option("timeStepDuration", "The elapsed time during a step of the simulation (default " + pilot.getTimeStepDuration() + ")");
        optionSimTimeStepDuration.setArgs(1);
        optionSimTimeStepDuration.setArgName("value");
        optionSimTimeStepDuration.setRequired(false);

        Option optionRNGStatusIndex = new Option("RNGStatusIndex", "The index of the status to init the RNG");
        optionRNGStatusIndex.setArgs(1);
        optionRNGStatusIndex.setArgName("index");
        optionRNGStatusIndex.setRequired(false);

        Options options = new Options();
        options.addOption(optionDebug);
        options.addOption(optionQuiet);
        options.addOption(optionEnvFiles);
        options.addOption(optionGroupsFiles);
        options.addOption(optionObserversFiles);
        options.addOption(optionSimBegin);
        options.addOption(optionSimDuration);
        options.addOption(optionSimTimeStepDuration);
        options.addOption(optionRNGStatusIndex);

        CommandLineParser parser = new BasicParser();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            cliError(options);
        }

        pilot.setDebug(cmd.hasOption(optionDebug.getOpt()));

        if (cmd.hasOption(optionQuiet.getOpt())) {
            pilot.setQuiet();
        }

        if (cmd.getOptionValue("simBegin") != null) {
            pilot.setSimBegin(Integer.parseInt(cmd.getOptionValue("simBegin")));
        }
        pilot.setSimDuration(Integer.parseInt(cmd.getOptionValue("simDuration")));
        if (cmd.getOptionValue("timeStepDuration") != null) {
            pilot.setSimTimeStepDuration(Integer.parseInt(cmd.getOptionValue("timeStepDuration")));
        }
        if (cmd.getOptionValue(optionEnvFiles.getOpt()) != null) {
            pilot.getAquaticWorld().setEnvironment(new FileReader(cmd.getOptionValue(optionEnvFiles.getOpt())));
        }
        if (cmd.getOptionValue(optionGroupsFiles.getOpt()) != null) {
            pilot.getAquaticWorld().setGroupsParameters(new FileReader(cmd.getOptionValue(optionGroupsFiles.getOpt())));

        }
        if (cmd.getOptionValue(optionObserversFiles.getOpt()) != null) {
            pilot.setObserversFile(new File(cmd.getOptionValue(optionObserversFiles.getOpt())));
        }

        if (cmd.getOptionValue(optionRNGStatusIndex.getOpt()) != null) {
            pilot.setRNGStatusIndex(Integer.parseInt(cmd.getOptionValue(optionRNGStatusIndex.getOpt())));
        }
        pilot.getOutputStream().print("Your command line arguments:\n\t");
        for (int i = 0; i < args.length; i++) {
            pilot.getOutputStream().print(args[i]);
            pilot.getOutputStream().print(" ");
        }
        pilot.getOutputStream().println();
    }

    public void cliError(Options options) {
        String message = "Error while parsing command line arguments";
        System.err.println(message);
        new HelpFormatter().printHelp(" ", options);
        throw new RuntimeException(message);
    }

    public static void main(String[] args) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Pilot pilot = new Pilot();
        BatchRunner runner = new BatchRunner(pilot);
        pilot.init();
        runner.parseArgs(args, false, true, false);
        pilot.load();
        pilot.run();
    }
}
