package fr.cemagref.simaqualife.extensions.spatial2D;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.cemagref.observation.gui.Configurable;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.ohoui.annotations.NoRecursive;
import fr.cemagref.ohoui.filters.NoTransientField;
import fr.cemagref.ohoui.swing.OhOUI;
import fr.cemagref.ohoui.swing.OhOUIDialog;
import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.pilot.Pilot;

@SuppressWarnings("serial")
@NoRecursive
public abstract class Movement2DObserver extends ObserverListener implements Configurable, fr.cemagref.observation.gui.Drawable {

    protected Pilot pilot;
    protected transient JComponent display;
    private transient List<AquaNismsGroup<? extends AquaNism<? extends Point2D, ?>, ?>> groups;
    private String title;
    @Description(name = "Groups colors", tooltip = "Colors af aquanisms groups")
    private List<Color> groupsColors;
    @Description(name = "Background color", tooltip = "Background color")
    private Color bgColor;
    @Description(name = "Environment color", tooltip = "")
    private Color envColor;
    @Description(name = "Cell size", tooltip = "The minimum size of a displayed cell")
    private int cellSizeMin;
    private transient int cellSize;
    private transient int displayWidthBak,  displayHeightBak;
    private transient double envAspectRatio,  rescaleFactor;

    public Movement2DObserver(Pilot pilot) {
        this(pilot, Color.WHITE);
    }

    public Movement2DObserver(Pilot pilot, Color bgColor) {
        super();
        this.pilot = pilot;
        this.bgColor = bgColor;
    }

    public abstract int getEnvWidth();

    public abstract int getEnvHeight();

    @Override
    public JComponent getDisplay() {
        return display;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long t) {
        display.repaint();
    }

    @Override
    public void init() {
        // init display component
        display = new JPanel(new BorderLayout());
        DisplayComponent displayComponent = new DisplayComponent();
        display.add(displayComponent, BorderLayout.CENTER);
        display.add(new JLabel("Movement 2D Observer"), BorderLayout.SOUTH);
        display.setVisible(true);
        display.setDoubleBuffered(true);

        // init display variables
        displayWidthBak = 0;
        displayHeightBak = 0;
        envAspectRatio = (double) getEnvWidth() / getEnvHeight();

        groups = new ArrayList<AquaNismsGroup<? extends AquaNism<? extends Point2D, ?>, ?>>(pilot.getAquaticWorld().getAquaNismsGroupsList());
        if (groups != null) {
            display.setVisible(true);
        }
        // init colors list, and put default
        groupsColors = new ArrayList<Color>();
        groupsColors.add(Color.RED);
        groupsColors.add(Color.GREEN);
        groupsColors.add(Color.YELLOW);
        groupsColors.add(Color.CYAN);
        groupsColors.add(Color.GRAY);

        // show the result
        display.repaint();
    }

    public void disable() {
        display.setVisible(false);
    }

    public void configure() {
        OhOUIDialog dialog = OhOUI.getDialog(null, this, new NoTransientField());
        dialog.setSize(new Dimension(500, 600));
        dialog.setVisible(true);
        display.repaint();
    }

    public void addObservable(ObservablesHandler classObservable) {
    // nothing to do
    }

    public void close() {
    // nothing to do        
    }

    /**
     * This method is called to draw each individual. You can override this method to
     * modify the way each individual is displayed.
     * 
     * @param displayComponent The Swing component that handle the display.
     * @param g The graphic context to use to draw primitives.
     * @param aquanism The individual to draw.
     * @param group	The group of the individual to draw.
     * @param c The color of the individual.
     */
    protected void drawIndividual(DisplayComponent displayComponent, Graphics2D g, AquaNism<? extends Point2D, ?> aquanism, AquaNismsGroup group, Color c) {
        drawCell(displayComponent, g, c, aquanism.getPosition().getX(), aquanism.getPosition().getY());
    }

    /**
     * This method is called to draw the background. You can override this method to
     * modify the way the background is displayed.
     * 
     * @param displayComponent The Swing component that handle the display.
     * @param g The graphic context to use to draw primitives.
     * @param c The color of the background as configured by the user.
     */
    protected void drawBackGround(DisplayComponent displayComponent, Graphics2D g, Color bgColor, Color envColor) {
        g.setColor(bgColor);
        g.fillRect(0, 0, displayComponent.getWidth(), displayComponent.getHeight());
        g.setColor(envColor);
        g.fillRect(0, 0, (int)(getEnvWidth() * rescaleFactor), (int)(getEnvHeight() * rescaleFactor));
    }

    protected void drawCell(DisplayComponent displayComponent, Graphics2D g, Color c, double x, double y) {
        g.setColor(c);
        Rectangle rect = new Rectangle((int) (rescaleFactor * x), displayComponent.getHeight() - (int) (rescaleFactor * y), cellSize, cellSize);
        g.fill(rect);
    }

    protected class DisplayComponent extends JComponent {

        @Override
        protected synchronized void paintComponent(Graphics g) {
            super.paintComponents(g);
            Graphics2D g2d = (Graphics2D) g;
            // determine if the display has been resized
            if ((this.getWidth() != displayWidthBak) || (this.getHeight() != displayHeightBak)) {
                // backup for comparaison in the next loop
                displayWidthBak = this.getWidth();
                displayHeightBak = this.getHeight();
                // compute the rescale factor with keeping the aspect ratio
                if (((double) displayWidthBak / displayHeightBak) < envAspectRatio) {
                    rescaleFactor = (double) displayWidthBak / getEnvWidth();
                } else {
                    rescaleFactor = (double) displayHeightBak / getEnvHeight();
                }
                cellSize = (rescaleFactor < cellSizeMin) ? cellSizeMin : (int) rescaleFactor;
            }
            // Draw Background
            drawBackGround(this, g2d, bgColor, envColor);
            // Draw aquanisms
            Iterator<Color> colorsIterator = groupsColors.iterator();
            Color currentColor = null;
            for (AquaNismsGroup<? extends AquaNism<? extends Point2D, ?>, ?> group : groups) {
                if (colorsIterator.hasNext()) {
                    currentColor = colorsIterator.next();
                }
                for (AquaNism<? extends Point2D, ?> aquanism : group.getAquaNismsList()) {
                    // draw the individuals
                    drawIndividual(this, g2d, aquanism, group, currentColor);
                }
            }
        }
    }
}
