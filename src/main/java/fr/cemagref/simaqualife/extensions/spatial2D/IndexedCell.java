package fr.cemagref.simaqualife.extensions.spatial2D;

import fr.cemagref.simaqualife.kernel.spatial.Position;

public class IndexedCell implements Position {
    
    protected int index;

    public IndexedCell(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    
}
