package fr.cemagref.simaqualife.extensions.spatial2D;

import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.spatial.Environment;

import java.util.ArrayList;
import java.util.List;

@Deprecated
/** Prefer to use Grid2D with TorusType enum
 * @deprecated
 */
public abstract class Grid2DTorus <IC extends IndexedCell, AN extends AquaNism<IC,?>> extends Environment<IC,AN> {
    
    protected transient IC[] grid;
    protected int gridWidth, gridHeight;
    protected NeighborsType neighborsType=Grid2DTorus.NeighborsType.NEIGHBORHOOD_MOORE; // by default

        public enum NeighborsType {
            NEIGHBORHOOD_VON_NEUMANN,  // 4 neighbors
            NEIGHBORHOOD_MOORE  // 8 neighbors
        }
	
	
    
    public List<IC> getVoisinage(IC position) {
        return getVoisinage(position.index);
    }

    /**
     * <code>getVoisinage</code> retrieves the height position around the position taken as argument.
     */
    public List<IC> getVoisinage(int index) {
        List<IC> voisinage = new ArrayList<IC>();
        int horizontalRest = index%gridWidth;
        if (neighborsType==NeighborsType.NEIGHBORHOOD_MOORE){
        // top - right - bottom - left
        boolean[] onBorder = 
            new boolean [] {index < gridWidth, horizontalRest == gridWidth-1, index >= gridWidth*(gridHeight-1), horizontalRest == 0};
        // top-left
        voisinage.add(grid[(onBorder[0] ? grid.length+index-gridWidth : index-gridWidth) + (onBorder[3] ? gridWidth-1 : -1)]);
        // top
        voisinage.add(grid[onBorder[0] ? grid.length+index-gridWidth : index-gridWidth]);
        // top-right
        voisinage.add(grid[(onBorder[0] ? grid.length+index-gridWidth : index-gridWidth) + (onBorder[1] ? 1-gridWidth : 1)]);
        // right
        voisinage.add(grid[index + (onBorder[1] ? 1-gridWidth: 1)]);
        // bottom-right
        voisinage.add(grid[(onBorder[2] ? horizontalRest : index+gridWidth) + (onBorder[1] ? 1-gridWidth : 1)]);
        // bottom
        voisinage.add(grid[onBorder[2] ? horizontalRest : index+gridWidth]);
        // bottom-left
        voisinage.add(grid[(onBorder[2] ? horizontalRest : index+gridWidth) + (onBorder[3] ? gridWidth-1 : -1)]);
        // left
        voisinage.add(grid[index + (onBorder[3] ? gridWidth-1 : -1) ]);
        }
        else {
            // @TODO
        }

        return voisinage;
    }
    
    /**
     * @param shiftX must be in [-1;1]
     * @param shiftY must be in [-1;1]
     */
    public IC getNeighbor(IC position, int shiftX, int shiftY) {
        return getNeighbor(position.index, shiftX, shiftY);
    }

    /**
     * @param shiftX must be in [-1;1]
     * @param shiftY must be in [-1;1]
     */
    public IC getNeighbor(int index, int shiftX, int shiftY) {
        int horizontalRest = index%gridWidth;
        // top - right - bottom - left
        boolean[] onBorder = 
            new boolean [] {index < gridWidth, horizontalRest == gridWidth-1, index >= gridWidth*(gridHeight-1), horizontalRest == 0};
        int resultIndex = index;
        if (shiftX == -1) {
            resultIndex -= 1;
            if (horizontalRest == 0)
                resultIndex += gridWidth;
        } else if (shiftX == 1) {
            resultIndex += 1;
            if (horizontalRest == gridWidth-1)
                resultIndex -= gridWidth;
        }
        if (shiftY == -1) {
            resultIndex -= gridWidth;
            if (index < gridWidth)
                resultIndex += grid.length;
        } else if (shiftY == 1) {
            resultIndex += gridWidth;
            if (index >= gridWidth*(gridHeight-1))
                resultIndex += horizontalRest-grid.length;
            else
                resultIndex += gridWidth;
        }
        return grid[resultIndex];
    }

    public IC[] getCells() {
        return grid;
    }
    
    public int getGridHeight() {
		return gridHeight;
	}

	public int getGridWidth() {
		return gridWidth;
	}

    public NeighborsType getNeighborsType() {
        return neighborsType;
    }

    public void setNeighborsType(NeighborsType neighborsType) {
        this.neighborsType = neighborsType;
    }
        

	@Override
    public void addAquaNism(AN aquaNism, AquaNismsGroup group) {
    }

    @Override
    public void removeAquaNism(AN aquaNism, AquaNismsGroup group) {
    }

    @Override
    public void moveAquaNism(AN aquaNism, AquaNismsGroup group, IC destination) {
    }
    

}
