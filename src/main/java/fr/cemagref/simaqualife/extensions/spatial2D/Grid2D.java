package fr.cemagref.simaqualife.extensions.spatial2D;

import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.spatial.Environment;
import fr.cemagref.simaqualife.kernel.util.TransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

import java.util.ArrayList;
import java.util.List;

/**
 * A Grid2D with no boundary
 * @author thierry
 * @param <IC>
 * @param <AN>
 */
public abstract class Grid2D<IC extends IndexedCell, AN extends AquaNism<IC, ?>> extends Environment<IC, AN> {

    protected transient IC[] grid;
    protected int gridWidth,  gridHeight;
    protected NeighborsType neighborsType = Grid2D.NeighborsType.NEIGHBORHOOD_VON_NEUMANN; // by default
    protected TorusType torusType = Grid2D.TorusType.NONE;
    protected transient int[][] pos = null;

    @TransientParameters.InitTransientParameters
    protected void initTransientParameters(Pilot pilot) {
        super.initTransientParametersPilot(pilot);
    }

    public enum TorusType {

        NORTH_SOUTH,
        EAST_WEST,
        BOTH,
        NONE
    }

    public enum NeighborsType {

        NEIGHBORHOOD_VON_NEUMANN(4, new int[][]{{0, -1, 1, 0}, {1, 0, 0, 1}}), // 4 neighbours
        NEIGHBORHOOD_MOORE(8, new int[][]{{-1, 0, 1, -1, 1, -1, 0, 1}, {-1, -1, -1, 0, 0, 1, 1, 1}}), // 8 neighbours
        HEXAGONAL(6, new int[][]{{-1, 0, 1, 0, -1}, {-1, -1, -1, 0, 1, 0}}), // Hexagonal (see example pour cell 4 (0,1;2,5,7,3)
        OTHER(0, null);  // other neighbours       _
//  0  1  2     For VON_NEUMANN : The neighbors of cell 4 are (1,3,5,7)
//  3  4  5     For MOORE : The neighbors of cell 4 are (0,1,2,3,5,6,7,8)
//  6  7  8       
//        //     _ / 1 \ _    The left 3 x 3 hexagonal grid. 
//   / 0 \ _ / 2 \  The first row of cells is 0, 1, 2 and (0,0) refers to cell 0, and (0,2) refers to cell 2. 
//   \ _ / 4 \ _ /  The next row of cells is 3, 4, 5, so (1,0) refers to cell 3 and so on. 
//   / 3 \ _ / 5 \  The neighbors with radius one arround cell 4 is composed of (0,1;2,5,7,3).
//   \ _ / 7 \ _ /  The grid wraps as a toriod such that cell (-1,0) refers to cell 2 and cell (0,-1) --> 6.
//   / 6 \ _ / 8 \
//   \ _ /   \ _ /

        //@TODO : how to take into account a generalized Moore with radius r or a generliezed Hexagonal with radius r
        private int[][] pos;

        NeighborsType(int value, int[][] pos) {
            this.value = value;
            this.pos = pos;
        }
        private final int value;

        public int getValue() {
            return value;
        }
    }

    public Grid2D(int gridWidth, int gridHeight, NeighborsType neighborsType) {
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.neighborsType = neighborsType;
        this.pos = neighborsType.pos;
    }

    public Grid2D(int gridWidth, int gridHeight, NeighborsType neighborsType,TorusType torusType) {
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.neighborsType = neighborsType;
        this.pos = neighborsType.pos;
        this.torusType=torusType;
    }
    
    @Override
    public List<IC> getNeighbours(IC position) {
        return getNeighbours(position.index);
    }

    /**
     * <code>getVoisinage</code> retrieves the height position around the position taken as argument.
     */
    public List<IC> getNeighbours(int index) {
        List<IC> neighbours = new ArrayList<IC>();

        for (int[] pos1 : pos) {
            IC neighbour = getNeighbour(index, pos1[0], pos1[1]);
            if (neighbour != null) {
                neighbours.add(neighbour);
            }
        }
        return neighbours;
    }

    /**
     * @param shiftX must be in [-1;1]
     * @param shiftY must be in [-1;1]
     */
    public IC getNeighbour(IC position, int shiftX, int shiftY) {
        return getNeighbour(position.index, shiftX, shiftY);
    }

    private static int mod(int a, int b) {
        int res = a % b;
        if (res < 0 && b > 0) {
            res += b;
        }
        return res;
    }

    /**
     * @param shiftX an int
     * @param shiftY an int
     */
    public IC getNeighbour(int index, int shiftX, int shiftY) {
        int horizontalRest = index % gridWidth + shiftX;
        int resultIndex = index;

        resultIndex += mod(horizontalRest, gridWidth);
        if ((horizontalRest < 0) | (horizontalRest > gridWidth - 1)) {
            if ((torusType != TorusType.EAST_WEST) & (torusType != TorusType.BOTH)) {
                return null;
            }
        }

        resultIndex += shiftY * gridWidth;
        if (resultIndex < 0 | resultIndex > grid.length - 1) {
            if ((torusType == TorusType.NORTH_SOUTH) | (torusType == TorusType.BOTH)) {
                resultIndex = mod(resultIndex, grid.length);
            } else {
                return null;
            }
        }


        return grid[resultIndex];
    }

    public IC[] getCells() {
        return grid;
    }

    public IC getCell(int index) {
        return grid[index];
    }

    public int getGridHeight() {
        return gridHeight;
    }

    public int getGridWidth() {
        return gridWidth;
    }

    public NeighborsType getNeighborsType() {
        return neighborsType;
    }

    public void setNeighborsType(NeighborsType neighborsType) {
        this.neighborsType = neighborsType;
    }

    @Override
    public void addAquaNism(AN aquaNism, AquaNismsGroup group) {
    }

    @Override
    public void removeAquaNism(AN aquaNism, AquaNismsGroup group) {
    }

    @Override
    public void moveAquaNism(AN aquaNism, AquaNismsGroup group, IC destination) {
    }
}
