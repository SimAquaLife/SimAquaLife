package fr.cemagref.simaqualife.extensions.spatial2D;


import fr.cemagref.simaqualife.kernel.spatial.Position;

import java.awt.Point;

public class Position2D extends Point implements Position /*implements Comparable<Position2D>*/ {
  
    public Position2D(int i, int j) {
        super(i,j);
    }

    public Position2D(Position2D position) {
        super(position);
    }

    public void setXY(int x,int y) {
        this.x = x;
        this.y = y;       
    }
    
    public String toString() {
    	return "("+x+","+y+")";
    }

    public double distanceWith(Position2D position) {
        return (position.x-x)*(position.x-x)+(position.y-y)*(position.y-y);
    }
    public Position2D getVectorTo(Position2D destination) {
        return new Position2D(destination.x-x,destination.y-y);
    }
    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Position2D position) {
        double d=distanceWith(position);
        // TODO qu'est-ce que c'est �a ?
        return (int)(Math.signum(d)+d);
    }
}

