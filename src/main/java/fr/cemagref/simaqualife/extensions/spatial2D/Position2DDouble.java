package fr.cemagref.simaqualife.extensions.spatial2D;

import java.awt.geom.Point2D;

import fr.cemagref.simaqualife.kernel.spatial.Position;

public class Position2DDouble extends Point2D.Double implements Position {

	public Position2DDouble(double x, double y) {
		super(x, y);
	}

}
